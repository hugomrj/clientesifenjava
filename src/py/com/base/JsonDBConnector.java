package py.com.base;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mysql.cj.jdbc.result.ResultSetMetaData;
import control.conexionBD;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.set.sifen.Util;

public class JsonDBConnector {

    //conexionBD conexion = new conexionBD();    
    Statement statement;
    ResultSet resultset;

    public String jsonEnviar(Integer Sucursal, String puntoExpedicion, Integer numeroVenta) {

        String sql = "";
        String sqldetalle = "";
        String jsonResult = "";

        sql = "SELECT * FROM \n"
                + " (   \n"
                + "   SELECT 1 as  iTipEmi, 'Normal' as dDesTipEmi, LPAD(FLOOR(RAND() * 1000000000), 9, '0') AS dCodSeg,\n"
                + "   1 as iTiDE, 'Factura electrónica' dDesTiDE, dNumTim,   "
                + "   LPAD(Sucursal, 3, '0') as dEst,  "
                + "   LPAD(puntoexpedicion, 3, '0') as dPunExp,\n"
                + "   LPAD(NumeroVenta, 7, '0') AS dNumDoc, '2023-09-25' dFeIniT ,\n"
                + "   CONCAT(fechaemision, 'T', horaemision) AS dFeEmiDE, 1 iTipTra, 'Venta de mercadería' dDesTipTra,\n"
                + "   1 iTImp, 'IVA' dDesTImp, 'PYG' cMoneOpe, 'Guarani' dDesMoneOpe,\n"
                + "   dRucEm, emisor.dDVEmi, iTipCont, dNomEmi, dNomFanEmi,dDirEmi, dNumCas, cDepEmi, dDesDepEmi, cDisEmi, dDesDisEmi,\n"
                + "   cCiuEmi, dDesCiuEmi, dTelEmi, dEmailE, cActEco, dDesActEco, iNatRec,\n"
                + "   2 iTiOpe, 'PRY' cPaisRec, 'Paraguay' dDesPaisRe,   \n"
                + "   CASE WHEN iNatRec = 1 THEN iTiContRec ELSE NULL END AS iTiContRec,\n"
                + "   CASE WHEN iNatRec = 1 THEN cedula ELSE NULL END AS dRucRec,\n"
                + "   CASE WHEN iNatRec = 1 THEN dDVRec ELSE NULL END AS dDVRec, \n"
                + "   CASE WHEN iNatRec = 2 THEN '1' ELSE NULL END AS iTipIDRec,\n"
                + "   CASE WHEN iNatRec = 2 THEN 'Cédula paraguaya' ELSE NULL END AS dDTipIDRec,\n"
                + "   CASE WHEN iNatRec = 2 THEN cedula ELSE NULL END AS dNumIDRec,\n"
                + "   razonsocial AS dNomRec, \n"
                + "   1 as iIndPres, 'Operación presencial' dDesIndPres, 1 as iCondOpe, 'Contado' dDCondOpe, \n"
                + "   1 as iTiPago, 'Efectivo' dDesTiPag, totalventa as dMonTiPag,\n"
                + "   'PYG' cMoneTiPag, 'Guarani' dDMoneTiPag\n"
                + "   FROM  ventas inner join cliente on (ventas.CodigoCliente = cliente.Codigo)\n"
                + "   inner join emisor on (emisor.Codigo = ventas.CodigoEmisor) \n"
                + "   where Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + " ) C1, \n"
                + " (\n"
                + "   select \n"
                + "   COALESCE(dSubExe, 0) dSubExe, COALESCE(dSubExo, 0) dSubExo, \n"
                + "   COALESCE(dSub5, 0) dSub5, COALESCE(dSub10, 0) dSub10,     \n"
                + "   COALESCE(t1.dSubExe, 0) + COALESCE(t2.dSubExo, 0) + COALESCE(t3.dSub5, 0) + COALESCE(t4.dSub10, 0) AS dTotOpe,\n"
                + "   0 dTotDesc, 0 dTotDescGlotem, 0 dTotAntItem, 0 dTotAnt, 0 dPorcDescTotal,\n"
                + "   0 dDescTotal, 0 dAnticipo, 0 dRedon,\n"
                + "   COALESCE(t1.dSubExe, 0) + COALESCE(t2.dSubExo, 0) + COALESCE(t3.dSub5, 0) + COALESCE(t4.dSub10, 0) AS dTotGralOpe,\n"
                + "   COALESCE(dIVA5, 0) dIVA5, COALESCE(dIVA10, 0) dIVA10,\n"
                + "   0 dLiqTotIVA5, 0 dLiqTotIVA10,\n"
                + "   COALESCE(dIVA5, 0) + COALESCE(dIVA10, 0) AS dTotIVA,\n"
                + "   COALESCE(dBaseGrav5, 0) dBaseGrav5,  COALESCE(dBaseGrav10, 0) dBaseGrav10,\n"
                + "   COALESCE(dBaseGrav5, 0) + COALESCE(dBaseGrav10, 0) AS dTBasGraIVA\n"
                + "    from \n"
                + "    (\n"
                + "        select Sucursal, PuntoExpedicion, NumeroVenta\n"
                + "        from ventas\n"
                + "        where Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "    ) as t0 left JOIN\n"
                + "    (\n"
                + "      select Sucursal,  PuntoExpedicion, NumeroVenta,  sum(subtotal) as dSubExe\n"
                + "      from detallesventas inner join productos\n"
                + "      on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "      inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "      where tiposimpuestos.Codigo = 3\n"
                + "      and Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "      group by Sucursal, PuntoExpedicion, NumeroVenta\n"
                + "    ) as t1  on ( t0.Sucursal = t1.Sucursal and t0.PuntoExpedicion = t1.PuntoExpedicion and  t0.PuntoExpedicion = t1.NumeroVenta)\n"
                + "    left join (\n"
                + "      select Sucursal, PuntoExpedicion, NumeroVenta,  sum(subtotal) as dSubExo\n"
                + "      from detallesventas inner join productos\n"
                + "      on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "      inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "      where tiposimpuestos.Codigo = 2\n"
                + "      and Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "      group by Sucursal, PuntoExpedicion, NumeroVenta\n"
                + "    ) as t2  on (t0.Sucursal = t1.Sucursal and t0.PuntoExpedicion = t2.PuntoExpedicion and  t0.NumeroVenta = t2.NumeroVenta)\n"
                + "    left join (\n"
                + "      select Sucursal, PuntoExpedicion, NumeroVenta,  sum(subtotal) as dSub5\n"
                + "      from detallesventas inner join productos\n"
                + "      on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "      inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "      where ( tiposimpuestos.Codigo = 1 or   tiposimpuestos.Codigo = 4 )\n"
                + "      and dTasaIVA = 5\n"
                + "      and Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "      group by Sucursal, PuntoExpedicion, NumeroVenta  \n"
                + "    ) as t3  on (t0.Sucursal = t1.Sucursal and t0.PuntoExpedicion = t3.PuntoExpedicion and  t0.NumeroVenta = t3.NumeroVenta)\n"
                + "    left join (\n"
                + "      select Sucursal, PuntoExpedicion, NumeroVenta,  sum(subtotal) as dSub10\n"
                + "      from detallesventas inner join productos\n"
                + "      on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "      inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "      where ( tiposimpuestos.Codigo = 1 or   tiposimpuestos.Codigo = 4 )\n"
                + "      and dTasaIVA = 10\n"
                + "      and Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "      group by Sucursal, PuntoExpedicion, NumeroVenta\n"
                + "    ) as t4  on (t0.Sucursal = t1.Sucursal and t0.PuntoExpedicion = t4.PuntoExpedicion and  t0.NumeroVenta = t4.NumeroVenta)\n"
                + "    left join (\n"
                + "      select Sucursal, PuntoExpedicion, NumeroVenta,  sum(TotalIva) as dIVA5\n"
                + "      from detallesventas inner join productos\n"
                + "      on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "      inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "      where ( tiposimpuestos.Codigo = 1 or   tiposimpuestos.Codigo = 4 )\n"
                + "      and dTasaIVA = 5\n"
                + "      and Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "      group by Sucursal, PuntoExpedicion, NumeroVenta\n"
                + "    ) as t5  on (t0.Sucursal = t1.Sucursal and t0.PuntoExpedicion = t5.PuntoExpedicion and  t0.NumeroVenta = t5.NumeroVenta)\n"
                + "    left join (\n"
                + "      select Sucursal, PuntoExpedicion, NumeroVenta,  sum(TotalIva) as dIVA10\n"
                + "      from detallesventas inner join productos\n"
                + "      on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "      inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "      where ( tiposimpuestos.Codigo = 1 or   tiposimpuestos.Codigo = 4 )\n"
                + "      and dTasaIVA = 10\n"
                + "      and Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "      group by Sucursal, PuntoExpedicion, NumeroVenta\n"
                + "    ) as t6  on (t0.Sucursal = t1.Sucursal and t0.PuntoExpedicion = t6.PuntoExpedicion and  t0.NumeroVenta = t6.NumeroVenta)\n"
                + "    left join (\n"
                + "      select Sucursal, PuntoExpedicion, NumeroVenta,  sum(TotalSinIva) as dBaseGrav5\n"
                + "      from detallesventas inner join productos\n"
                + "      on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "      inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "      where ( tiposimpuestos.Codigo = 1 or   tiposimpuestos.Codigo = 4 )\n"
                + "      and dTasaIVA = 5\n"
                + "      and Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "      group by Sucursal, PuntoExpedicion, NumeroVenta  \n"
                + "    ) as t7  on (t0.Sucursal = t1.Sucursal and  t0.PuntoExpedicion = t7.PuntoExpedicion and  t0.NumeroVenta = t7.NumeroVenta)\n"
                + "    left join (\n"
                + "      select Sucursal, PuntoExpedicion, NumeroVenta,  sum(TotalSinIva) as dBaseGrav10\n"
                + "      from detallesventas inner join productos\n"
                + "      on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "      inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "      where ( tiposimpuestos.Codigo = 1 or   tiposimpuestos.Codigo = 4 )\n"
                + "      and dTasaIVA = 10\n"
                + "      and Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3\n"
                + "      group by Sucursal, PuntoExpedicion, NumeroVenta  \n"
                + "    ) as t8  on (t0.Sucursal = t1.Sucursal and t0.PuntoExpedicion = t8.PuntoExpedicion and  t0.NumeroVenta = t8.NumeroVenta)\n"
                + " ) C2 ;";

        sql = sql.replaceAll("#1", Sucursal.toString())
                .replaceAll("#2",  puntoExpedicion 
                .replaceAll("#3", numeroVenta.toString()));

//JsonArray detalle = this.jsonArray(sqldetalle);
        sqldetalle = "select  codigoproducto dCodInt, productos.descripcion dDesProSer, \n"
                + "unidadesmedida.Codigo as cUniMed, unidadesmedida.Representacion dDesUniMed,\n"
                + "detallesventas.cantidad dCantProSer, precio dPUniProSer, subtotal dTotBruOpeItem,\n"
                + "subtotal dTotOpeItem, tiposimpuestos.Codigo iAfecIVA, tiposimpuestos.nombre dDesAfecIVA,\n"
                + "100 as dPropIVA, dTasaIVA,\n"
                + "TotalSinIva as dBasGravIVA, TotalIva  as dLiqIVAItem, dBasExe\n"
                + "from detallesventas inner join productos\n"
                + "on (detallesventas.CodigoProducto = productos.Codigo)\n"
                + "inner join unidadesmedida on (unidadesmedida.Codigo = productos.CodigoMedida)\n"
                + "inner join tiposimpuestos on (tiposimpuestos.Codigo = productos.CodigoImpuesto)\n"
                + "where Sucursal = #1  and  PuntoExpedicion like '#2'  and NumeroVenta = #3;";

        sqldetalle = sqldetalle.replaceAll("#1", Sucursal.toString())
                .replaceAll("#2",  puntoExpedicion 
                .replaceAll("#3", numeroVenta.toString()));

        JsonObject jsonObject = null;

        try {

            JsonArray detalle = this.jsonArray(sqldetalle);

            jsonObject = this.jsonObjet(sql);

            jsonObject.add("Detalles", detalle);

            jsonResult = jsonObject.toString();

            //  System.out.println(detalle);
        } catch (Exception ex) {
            Logger.getLogger(JsonDBConnector.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            return jsonResult;
        }

    }

    public JsonObject jsonObjet(String sql) throws Exception {

        conexionBD conexion = new conexionBD();

        statement = conexion.getConnection().createStatement();

        resultset = statement.executeQuery(sql);

        JsonObject jsonObject = resultSetToJsonObject(resultset);

        conexion.cerrar_conexion();
        return jsonObject;

    }

    public JsonArray jsonArray(String sql) throws Exception {

        conexionBD conexion = new conexionBD();
        statement = conexion.getConnection().createStatement();
        resultset = statement.executeQuery(sql);

        JsonArray jsonArray = new JsonArray();

        while (resultset.next()) {
            JsonObject jsonObject = new JsonObject();
            int totalColumns = resultset.getMetaData().getColumnCount();
            for (int i = 1; i <= totalColumns; i++) {

                String propiedadvalor = resultset.getObject(i).toString();
                propiedadvalor = Util.convertIfScientific(propiedadvalor);
                jsonObject.addProperty(resultset.getMetaData().getColumnLabel(i), propiedadvalor);
                // System.out.println( propiedadvalor  );
            }
            jsonArray.add(jsonObject);
        }

        return jsonArray;

    }

    public static JsonObject resultSetToJsonObject(ResultSet resultSet) throws Exception {

        JsonObject jsonObject = new JsonObject();
        ResultSetMetaData metaData = (ResultSetMetaData) resultSet.getMetaData();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

        if (resultSet.next()) {
            int columnCount = metaData.getColumnCount();

            for (int i = 1; i <= columnCount; i++) {
                //String columnName = metaData.getColumnName(i);
                String columnName = metaData.getColumnLabel(i);
                Object columnValue = resultSet.getObject(i);

                if (columnValue != null) {
                    addPropertyToJson(jsonObject, columnName, columnValue, dateFormat, timeFormat);
                }

                /*
                if (columnValue == null) {
                    jsonObject.add(columnName, null);
                } else {
                    addPropertyToJson(jsonObject, columnName, columnValue, dateFormat, timeFormat);
                }
                 */
            }
        } else {
            System.out.println("No se encontraron registros");
        }

        return jsonObject;
    }

    public static void addPropertyToJson(JsonObject jsonObject, String columnName,
            Object columnValue, SimpleDateFormat dateFormat, SimpleDateFormat timeFormat)
            throws Exception {

        if (columnValue != null) {

            if (columnValue instanceof Integer) {
                jsonObject.addProperty(columnName, (Integer) columnValue);
            } else if (columnValue instanceof String) {
                jsonObject.addProperty(columnName, (String) columnValue);
            } else if (columnValue instanceof Double) {
                jsonObject.addProperty(columnName,   Util.convertIfScientific(columnValue.toString()) );
            } else if (columnValue instanceof Float) {
                jsonObject.addProperty(columnName, (Float) columnValue);
            } else if (columnValue instanceof Long) {
                jsonObject.addProperty(columnName, (Long) columnValue);
            } else if (columnValue instanceof Boolean) {
                jsonObject.addProperty(columnName, (Boolean) columnValue);
            } else if (columnValue instanceof Byte) {
                jsonObject.addProperty(columnName, (Byte) columnValue);
            } else if (columnValue instanceof Short) {
                jsonObject.addProperty(columnName, (Short) columnValue);
            } else if (columnValue instanceof byte[]) {
                jsonObject.addProperty(columnName, new String((byte[]) columnValue));
            } else if (columnValue instanceof java.sql.Date) {
                String fechaFormateada = dateFormat.format((java.sql.Date) columnValue);
                jsonObject.addProperty(columnName, fechaFormateada);
            } else if (columnValue instanceof java.sql.Time) {
                String horaFormateada = timeFormat.format((java.sql.Time) columnValue);
                jsonObject.addProperty(columnName, horaFormateada);
            } else if (columnValue instanceof java.math.BigDecimal) {
                jsonObject.addProperty(columnName, (java.math.BigDecimal) columnValue);
            } else {
                throw new Exception("Tipo de datos no compatible: " + columnValue.getClass());
            }
//            System.out.println("--");
//            System.out.println("Nombre de columna: " + columnName);
//            System.out.println("Tipo de dato: " + columnValue.getClass().getSimpleName());
//            System.out.println("Valor: " + columnValue);
//            System.out.println("--");
        }

    }

}
