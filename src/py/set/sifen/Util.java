/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.set.sifen;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*
 * @author hugom_000
 */


public  abstract class Util {
    
    
    public static String convertIfScientific(String value) {
        // Expresión regular para verificar si el string está en notación científica
        String scientificNotationPattern = "-?\\d+(\\.\\d+)?([eE][+-]?\\d+)?";
        Pattern pattern = Pattern.compile(scientificNotationPattern);
        Matcher matcher = pattern.matcher(value);
        // Verificar si el string está en notación científica
        if (matcher.matches()) {
            // Convertir el string en notación científica a BigDecimal y luego a un string sin notación científica
            return new BigDecimal(value).toPlainString();
        } else {
            // Si no está en notación científica, devolver el mismo string
            return value;
        }
    }   
       
    
    
    
    
    
}
