package test.prueba;


import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import py.set.sifen.Util;

public class Pruebacientifico {


   public static void main(String[] args) {
        // Ejemplo de un valor en notación científica
        String stringValue1 = "1.0E7";
        String stringValue2 = "MERCADERIAS VARIOS";

        // Verificar si el string está en notación científica y convertir si es necesario
        String result1 = Util.convertIfScientific(stringValue1);
        String result2 = Util.convertIfScientific(stringValue2);

        System.out.println("Resultado 1: " + result1);
        System.out.println("Resultado 2: " + result2);
    }

   

   
    
}
